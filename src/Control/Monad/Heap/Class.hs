{-# LANGUAGE FunctionalDependencies, MultiParamTypeClasses #-}

module Control.Monad.Heap.Class where

import Data.Elem

class Monad m => MonadHeap m ts r | m -> ts r where
  newRef :: HasElem a ts => a -> m (r a)
  readRef :: HasElem a ts => r a -> m a
  writeRef :: HasElem a ts => r a -> a -> m ()
  modifyRef :: HasElem a ts => r a -> (a -> a) -> m ()
  modifyRef ref fun = do
    val <- readRef ref
    writeRef ref (fun val)
