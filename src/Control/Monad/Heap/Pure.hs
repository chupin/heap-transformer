{-# LANGUAGE BangPatterns, DataKinds, FlexibleContexts, FlexibleInstances,
             GADTs, GeneralizedNewtypeDeriving, KindSignatures,
             MultiParamTypeClasses, OverloadedLists, PolyKinds, RankNTypes,
             TypeFamilies, TypeOperators, UndecidableInstances #-}

-- | An pure 'HeapT' transformer. It is a safe transformer for the 'Control.Monad.ST.ST' monad.
--
-- Unlike the 'Control.Monad.ST.ST' monad, it cannot produce references to values of any type: it can only contain references to values whose types have been specified in a type-level list parametrizing 'HeapT'

module Control.Monad.Heap.Pure ( Ref
                               , HeapT
                               , runHeapT
                               , newHeapRef
                               , readHeapRef
                               , writeHeapRef
                               , writeHeapRef'
                               , modifyHeapRef
                               , modifyHeapRef'
                               , Elem(..)
                               , HasElem
                               , InitHeaps
                               , Erased
                               , erase
                               , runErased
                               , module Control.Monad.Heap.Class
                               ) where

import           Control.Monad.Heap.Class
import           Data.Elem

import           Control.Applicative
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Control.Monad.Writer
import           Data.IntMap                (IntMap)
import qualified Data.IntMap                as M
import qualified Data.IntMap.Strict         as MS

-- | The type of references. It carries the type of the value it points to as well as a phantom type to prevent it from leaking outside the computation it was created in. Internally, it is simply a wrapper around an 'Int'.
newtype Ref s a = Ref Int

instance Eq (Ref s a) where
  Ref n == Ref m = n == m

instance Ord (Ref s a) where
  Ref n `compare` Ref m = n `compare` m

-- | A 'Heap' containing values of type @a@. It contains a dictionnary indexed by integers as well as a an integer sourece, giving the value of the next free reference.
data Heap s a = Heap { heap      :: !(IntMap a)
                     , refSource :: !Int
                     }

-- | A list containing 'Heap's pointing to different types of value. The type level list is the list of the different types the @Heap@s contain.
data Heaps :: * -> [*] -> * where
  Bottom :: Heaps s '[]
  Push :: !(Heap s a) -> !(Heaps s as) -> Heaps s (a ': as)

-- | The 'HeapT' monad transformer. The type-level list @ts@ is the list of type it is possible to have references to.
newtype HeapT s (ts :: [*]) (m :: * -> *) a = HeapT (StateT (Heaps s ts) m a)
                                            deriving( Functor
                                                    , Applicative
                                                    , Monad
                                                    , MonadTrans
                                                    , Alternative
                                                    , MonadPlus
                                                    )

instance {-# OVERLAPS #-} Monad m => MonadHeap (HeapT s ts m) ts (Ref s) where
  newRef = newHeapRef
  readRef = readHeapRef
  writeRef = writeHeapRef
  modifyRef = modifyHeapRef

instance (MonadTrans t, MonadHeap m ts r, Monad (t m)) =>
         MonadHeap (t m) ts r where
  newRef = lift . newRef
  readRef = lift . readRef
  writeRef r v = lift (writeRef r v)
  modifyRef r f = lift (modifyRef r f)

instance MonadReader r m => MonadReader r (HeapT s ts m) where
  ask = lift ask
  local f (HeapT a) = HeapT (local f a)

instance MonadWriter r m => MonadWriter r (HeapT s ts m) where
  writer = lift . writer
  listen (HeapT a) = HeapT (listen a)
  pass (HeapT a) = HeapT (pass a)

instance MonadState r m => MonadState r (HeapT s ts m) where
  get = lift get
  put = lift . put

-- | A helper function to implement 'newHeapRef', 'readHeapRef', etc.
--
-- Initially there were no bangs, so the tuples weren't forced. That
-- meant that even if we called modifyHeap and forced evaluation of
-- its result to WHNF, fun heap (or the recursive call to modifyHeap)
-- wouldn't be evaluated to WHNF. In the case of the readHeapRef
-- function (see line 140), this meant that we had to retain a
-- reference on the current dictionnary. Now though, because we force
-- evaluation of fun heap to WHNF before producing the result, the
-- call to modHeap will have performed the lookup and the reference to
-- the old dictionnary can be dropped.
modifyHeap :: Elem a as
           -> (Heap s a -> (b, Heap s a))
           -> Heaps s as
           -> (b, Heaps s as)
modifyHeap Here fun (Push heap heaps) = (res, Push newHeap heaps)
  where !(res, newHeap) = fun heap
modifyHeap (There later) fun (Push heap heaps) = (res, Push heap newHeaps)
  where !(res, newHeaps) = modifyHeap later fun heaps

-- | I wrote the helper function below (readHeap) when I was searching for the space leak. It seems to be slightly more efficient (~5-10%) for implementing readHeapRef. I'll investigate.
readHeap :: Elem a as
         -> Heaps s as
         -> Ref s a
         -> Box a
readHeap Here (Push Heap { heap = heap } _) (Ref idx)     =
  case M.lookup idx heap of
    Nothing  -> error "Ooopsie"
    Just val -> Box val
readHeap (There later) (Push _ heaps) ref = readHeap later heaps ref

-- | The box is necessary to force evaluation of the case (and avoid holding a reference on an old dictionnary) while not forcing the value.
data Box a = Box a

-- | Create a new reference. It is necessary for the type @a@ to be in the list parametrizing @HeapT@.
newHeapRef :: (Monad m, HasElem a as) => a -> HeapT s as m (Ref s a)
newHeapRef val =
  HeapT $ do
  heaps <- get
  let modHeap Heap { heap = heap
                   , refSource = idx
                   } =
        (Ref idx, Heap { heap = M.insert idx val heap
                       , refSource = idx + 1
                       })
  case modifyHeap elemProof modHeap heaps of
    (ref, newHeaps) -> do
      put newHeaps
      pure ref

-- | Read the value associated to a reference.
readHeapRef :: (Monad m, HasElem a as) => Ref s a -> HeapT s as m a
readHeapRef (Ref idx) =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap } =
        case M.lookup idx heap of
          Nothing  -> error "Ooopsie" -- I'm good at error messages
          Just val -> (val, hp)
      !(res, _) = modifyHeap elemProof modHeap heaps
      -- !(Box res) = readHeap elemProof heaps (Ref idx)
  pure res

-- | Overwrite the value associated to a reference.
writeHeapRef :: (Monad m, HasElem a as) => Ref s a -> a -> HeapT s as m ()
writeHeapRef ref@(Ref idx) val =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap } =
        ((), hp { heap = M.insert idx val heap })
      !((), newHeaps) = modifyHeap elemProof modHeap heaps
  put newHeaps

-- | Overwrite the value associated to a reference, evaluating the written value to weak head normal form.
writeHeapRef' :: (Monad m, HasElem a as) => Ref s a -> a -> HeapT s as m ()
writeHeapRef' ref@(Ref idx) !val =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap } =
        ((), hp { heap = M.insert idx val heap })
  case modifyHeap elemProof modHeap heaps of
    ((), newHeaps) -> put newHeaps

-- | Modify the value associated to a reference.
modifyHeapRef :: (Monad m, HasElem a as) => Ref s a -> (a -> a) -> HeapT s as m ()
modifyHeapRef (Ref idx) fun =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap } =
        ((), hp { heap = M.adjust fun idx heap })
      !((), newHeaps) = modifyHeap elemProof modHeap heaps
  put newHeaps

  -- do
  -- val <- readHeapRef ref
  -- writeHeapRef ref $ fun val

-- | Strict version of 'modifyHeapRef'
modifyHeapRef' :: (Monad m, HasElem a as) => Ref s a -> (a -> a) -> HeapT s as m ()
modifyHeapRef' (Ref idx) fun =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap } =
        ((), hp { heap = MS.adjust fun idx heap })
      !((), newHeaps) = modifyHeap elemProof modHeap heaps
  put newHeaps

  -- HeapT $ do
  -- heaps <- get
  -- let modHeap hp@Heap { heap = heap } =
  -- -- do
  -- -- val <- readHeapRef ref
  -- -- writeHeapRef ref $! fun val

---------------------------------------------------------------------------
-- Running HeapT
---------------------------------------------------------------------------

-- | Allows us to generate a list of empty heaps from the type-level list @ts@. Any type-level list of type is an instance of 'InitHeaps'.
class InitHeaps (ts :: [*]) where
  -- | Generates a list of empty heaps corresponding to @ts@
  mkHeaps :: Heaps s ts

instance InitHeaps '[] where
  mkHeaps = Bottom

instance InitHeaps ts => InitHeaps (t : ts) where
  mkHeaps = Push Heap { heap = []
                      , refSource = 0
                      } mkHeaps

-- | Run a 'HeapT'. The rank-2 type prevents to leak values depending on @s@, such as 'Ref', to the outside.
runHeapT :: (Monad m, InitHeaps ts)
         => (forall s. HeapT s ts m a)
         -> m a
runHeapT (HeapT act) = evalStateT act mkHeaps

-- There's a bit of magic left. Consider we're writing a typechecker
-- for the simply typed lambda calculus with destructive unification
-- (where the type variables carry a reference to the eventual other
-- type they've been unified with). Here's the the type of types:
--
-- data Type s = TVar { varIndex :: Int
--                    , varInst  :: Ref s (Maybe (Type s))
--                    }
--             | TFun (Type s) (Type s)
--
-- The type checker will be an action of a type like this:
--
-- HeapT s [Type s] m a
--
-- But one can't call runHeapT on a value of this type, because ts =
-- [Type s] depends on s. However it shouldn't matter, because the
-- only thing that shouldn't depend on s is a. The solution is then to
-- erase the list of types from HeapT as follow:

-- | 'Erased' takes a 'HeapT' and erases the type-level list of types. This is useful is the type-level list references the phantom type @s@, thus preventing us to call 'runHeapT'.
data Erased s m a where
  Erase :: InitHeaps ts => HeapT s ts m a -> Erased s m a

-- | Erase the type-level list
erase :: InitHeaps ts => HeapT s ts m a -> Erased s m a
erase = Erase

-- | Run an 'Erased'
runErased :: Monad m
          => (forall s. Erased s m a)
          -> m a
runErased (Erase (HeapT heapT)) = evalStateT heapT mkHeaps
