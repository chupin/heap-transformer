{-# LANGUAGE DataKinds, FlexibleInstances, GADTs, KindSignatures, MultiParamTypeClasses, PolyKinds,
             TypeOperators #-}

module Data.Elem ( Elem(..)
                 , HasElem(..)
                 ) where

-- | A proof that a list contains a certain value
data Elem :: a -> [a] -> * where
  -- | @x@ is present in any list its the head of
  Here :: Elem x (x : xs)
  -- | If @x@ is present in @xs@, then its present in any list whose tail is @xs@
  There :: Elem x xs -> Elem x (y : xs)

-- | This class automatically generate an element of 'Elem'.
--
-- It will generate awful error messages if you try to use one of the above function and the list of types doesn't contain the type you're using. This could be of help when and if it gets merged: https://ghc.haskell.org/trac/ghc/wiki/Proposal/CustomTypeErrors
class HasElem (x :: a) (xs :: [a]) where
  -- | The auto-generated proof
  elemProof :: Elem x xs

instance {-# OVERLAPS #-} HasElem x (x : xs) where
  elemProof = Here

instance HasElem x xs => HasElem x (y : xs) where
  elemProof = There elemProof
